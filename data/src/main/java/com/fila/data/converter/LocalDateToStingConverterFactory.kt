package ru.x5.gkmobile.data.api.converter

import com.google.gson.Gson
import org.threeten.bp.LocalDate
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

class LocalDateToStringConverterFactory(gson: Gson) : Converter.Factory() {

    private val converter = Converter<LocalDate, String> {
        gson.toJson(it).run { replace("\"", "") }
    }

    override fun stringConverter(type: Type?, annotations: Array<out Annotation>?, retrofit: Retrofit?): Converter<*, String>? =
            if (type == LocalDate::class.java) converter
            else null
}