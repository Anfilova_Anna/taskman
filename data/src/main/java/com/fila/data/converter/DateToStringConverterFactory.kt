package ru.x5.gkmobile.data.api.converter

import com.google.gson.Gson
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type
import java.util.*

class DateToStringConverterFactory(gson: Gson) : Converter.Factory() {

    private val converter = Converter<Date, String> {
        gson.toJson(it).run { replace("\"", "") }
    }

    override fun stringConverter(type: Type?, annotations: Array<out Annotation>?, retrofit: Retrofit?): Converter<*, String>? =
            if (type == Date::class.java) converter
            else null
}