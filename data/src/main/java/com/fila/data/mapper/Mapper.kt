package com.fila.data.mapper

interface Mapper<TSource, TResult> {
    fun map(s: TSource): TResult
}
interface RepositoryMapper<TDomainEntity, TRepositiotyEntity> {
    fun mapToRepository(t: TDomainEntity): TRepositiotyEntity
}