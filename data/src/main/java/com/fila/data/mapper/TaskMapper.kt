package com.fila.data.mapper

import com.fila.data.model.tasks.TaskResponse
import com.fila.domain.fromOrdinal
import com.fila.domain.model.Task
import com.fila.domain.toLocalDate

class TaskMapper(private val tagMapper: TagMapper) : Mapper<TaskResponse, Task> {

    override fun map(s: TaskResponse): Task {
        return Task(name = s.name,
                id = s.id,
                creationTime = s.creationTime.toLocalDate(),
                comment = s.comment,
                notificationEnabled = s.notificationEnabled,
                dueToDate = s.dueToDate?.toLocalDate(),
                priority = fromOrdinal(s.priority),
                taskListId = s.listId,
                tags = s.tags.map(tagMapper::map)
        )
    }
}