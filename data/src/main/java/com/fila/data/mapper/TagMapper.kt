package com.fila.data.mapper

import com.fila.data.model.tags.TagResponse
import com.fila.domain.model.Tag

class TagMapper : Mapper<TagResponse, Tag> {
    override fun map(s: TagResponse): Tag {
        return Tag(
                name = s.name,
                id = s.id,
                info = s.info,
                iconId = s.iconId
        )
    }
}