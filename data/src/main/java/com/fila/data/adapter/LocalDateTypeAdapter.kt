package com.fila.data.adapter

import com.google.gson.*
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.lang.reflect.Type
import java.util.*

class LocalDateTypeAdapter(val format: String) : JsonSerializer<LocalDate>, JsonDeserializer<LocalDate> {

    private val formatter = DateTimeFormatter.ofPattern(format, Locale.getDefault())

    override fun serialize(src: LocalDate?, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement =
            src?.atStartOfDay()?.format(formatter)?.let { JsonPrimitive(it) } ?: JsonNull.INSTANCE


    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): LocalDate? =
            json?.asString?.let { formatter.parse(it) }?.let { LocalDate.from(it) }
}