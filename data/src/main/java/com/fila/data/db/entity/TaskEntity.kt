package com.fila.data.db.entity

import android.arch.persistence.room.*
import com.fila.data.db.Converters
import com.fila.data.db.entity.TaskEntity.Companion.ID_COLUMN
import com.fila.data.db.entity.TaskEntity.Companion.TABLE_NAME
import java.util.*

@Entity(tableName = TABLE_NAME, primaryKeys = [ID_COLUMN])
@TypeConverters(Converters::class)
data class TaskEntity(
        @ColumnInfo(name = ID_COLUMN)
        val id: Long,
        @ColumnInfo(name = LIST_ID_COLUMN)
        val listId: Long,
        @ColumnInfo(name = TASK_NAME_COLUMN)
        val name: String,
        @ColumnInfo(name = TASK_COMMENT_COLUMN)
        val comment: String?,
        @ColumnInfo(name = CREATION_TIME_COLUMN)
        val creationTime: Date,
        @ColumnInfo(name = DUE_DATE_COLUMN)
        val dueDate: Date?,
        @ColumnInfo(name = NOTIFICATION_ENABLED_COLUMN)
        val notificationEnabled: Boolean,
        @ColumnInfo(name = PRIORITY_COLUMN)
        val priority: Int,
        @ColumnInfo(name = TAG_ID_LIST_COLUMN)
        val tagIdList: List<Int>?
) {
    companion object {
        const val TABLE_NAME = "tasks"
        const val ID_COLUMN = "id"
        const val LIST_ID_COLUMN = "listId"
        const val TASK_NAME_COLUMN = "taskName"
        const val TASK_COMMENT_COLUMN = "taskComment"
        const val CREATION_TIME_COLUMN = "creationTime"
        const val DUE_DATE_COLUMN = "dueDate"
        const val NOTIFICATION_ENABLED_COLUMN = "notificationFlag"
        const val PRIORITY_COLUMN = "priority"
        const val TAG_ID_LIST_COLUMN = "tagId"
    }
}