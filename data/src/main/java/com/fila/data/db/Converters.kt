package com.fila.data.db

import android.arch.persistence.room.TypeConverter
import com.fila.domain.toEpochMilli
import com.fila.domain.toLocalDateTime
import org.threeten.bp.Instant
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import java.util.*

class Converters {
    companion object {
        @TypeConverter
        @JvmStatic
        fun datefromTimestamp(value: Long?): Date? {
            return value?.let { Date(value) }
        }

        @TypeConverter
        @JvmStatic
        fun dateToTimestamp(date: Date?): Long? {
            return date?.time
        }

        @TypeConverter
        @JvmStatic
        fun fromList(list: List<String>?): String {
            return list?.joinToString(",") ?: ""
        }

        @TypeConverter
        @JvmStatic
        fun toList(data: String?): List<String> {
            return data?.split(",") ?: emptyList()
        }

        @TypeConverter
        @JvmStatic
        fun toSet(data: String?): Set<String> {
            return toList(data).toSet()
        }

        @TypeConverter
        @JvmStatic
        fun fromIntList(list: List<Int>?): String {
            return list?.joinToString(",") ?: ""
        }

        @TypeConverter
        @JvmStatic
        fun fromLongList(list: List<Long>?): String {
            return list?.joinToString(",") ?: ""
        }

        @TypeConverter
        @JvmStatic
        fun toIntList(data: String?): List<Int> {
            return data?.split(",")?.map { it.toInt() } ?: emptyList()
        }

        @TypeConverter
        @JvmStatic
        fun localDateFromTimeStamp(value: Long?): LocalDate? {
            return value?.let { LocalDate.ofEpochDay(it) }
        }

        @TypeConverter
        @JvmStatic
        fun localDateToTimestamp(date: LocalDate?): Long? {
            return date?.toEpochDay()
        }

        @TypeConverter
        @JvmStatic
        fun localDateTimeFromTimeStamp(value: Long?): LocalDateTime? {
            return value?.let { Instant.ofEpochMilli(value).toLocalDateTime() }
        }

        @TypeConverter
        @JvmStatic
        fun localDateTimeToTimestamp(date: LocalDateTime?): Long? {
            return date?.toEpochMilli()
        }
    }
}