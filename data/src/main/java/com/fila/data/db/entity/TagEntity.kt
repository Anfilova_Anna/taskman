package com.fila.data.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = TagEntity.tableName)
data class TagEntity(
        @ColumnInfo(name = TAG_NAME_COLUMN)
        val name: String,
        @ColumnInfo(name = TAG_ID_COLUMN)
        @PrimaryKey
        val id: Int,
        @ColumnInfo(name = TAG_INFO_COLUMN)
        val info: String? = null,
        @ColumnInfo(name = ICON_ID_COLUMN)
        val iconId: Int? = null
) {
    companion object {
        const val tableName = "tags"
        const val TAG_NAME_COLUMN = "tagName"
        const val TAG_ID_COLUMN = "tagId"
        const val TAG_INFO_COLUMN = "tagInfo"
        const val ICON_ID_COLUMN = "tagIcon"
    }
}

// indices = [Index(value = [TAG_ID_LIST_COLUMN], unique = true)]