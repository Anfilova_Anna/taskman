package com.fila.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.fila.data.db.entity.TagEntity
import com.fila.data.db.entity.TaskEntity

@Database(entities = [TaskEntity::class, TagEntity:: class], version = DATABASE_VERSION, exportSchema = false) // TODO
@TypeConverters(Converters::class)
abstract class TaskManDb : RoomDatabase() {
    abstract fun taskDao(): TaskDao
    abstract fun tagDao() : TagDao
}