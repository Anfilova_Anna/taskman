package com.fila.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.fila.data.db.entity.TaskEntity

@Dao
interface TaskDao {

    @Insert(onConflict = REPLACE)
    fun insertTask(task: TaskEntity)

    @Insert(onConflict = REPLACE)
    fun insertTasks(tasks: List<TaskEntity>)

    @Query("SELECT * from tasks ORDER BY creationTime ASC")
    fun getAllTasks(): List<TaskEntity>

}