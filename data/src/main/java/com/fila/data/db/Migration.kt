package com.fila.data.db

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.migration.Migration
import com.fila.data.db.entity.TaskEntity
import com.fila.data.db.entity.TaskEntity.Companion.PRIORITY_COLUMN

object MigrationSet {

    val MIGRATION_1_2 = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE ${TaskEntity.TABLE_NAME} ADD COLUMN $PRIORITY_COLUMN INTEGER NOT NULL DEFAULT 0")
        }
    }

    val migrationSet = arrayOf(MIGRATION_1_2)
}

