package com.fila.data.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import com.fila.data.db.entity.TagEntity

@Dao
interface TagDao {

    @Insert(onConflict = REPLACE)
    fun insertTag(task: TagEntity)

    @Insert(onConflict = REPLACE)
    fun insertTags(tasks: List<TagEntity>)

    @Query("SELECT * from tags")
    fun getAllTags(): List<TagEntity>

    @Query("SELECT * from tags WHERE tagId IN(:idList)")
    fun getAllTagsById(idList: List<Int>): List<TagEntity>
}