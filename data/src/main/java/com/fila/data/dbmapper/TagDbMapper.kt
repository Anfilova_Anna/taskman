package com.fila.data.dbmapper

import com.fila.data.db.entity.TagEntity
import com.fila.data.mapper.Mapper
import com.fila.data.mapper.RepositoryMapper
import com.fila.domain.model.Tag

class TagDbMapper : Mapper<TagEntity, Tag>, RepositoryMapper<Tag, TagEntity> {
    override fun mapToRepository(t: Tag): TagEntity {
        return TagEntity(
                name = t.name,
                id = t.id,
                info = t.info,
                iconId = t.iconId
        )
    }

    override fun map(s: TagEntity): Tag {
        return Tag(
                name = s.name,
                id = s.id,
                info = s.info,
                iconId = s.iconId)
    }
}