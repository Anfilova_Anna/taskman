package com.fila.data.dbmapper

import com.fila.data.db.entity.TagEntity
import com.fila.data.db.entity.TaskEntity
import com.fila.data.mapper.Mapper
import com.fila.data.mapper.RepositoryMapper
import com.fila.domain.fromOrdinal
import com.fila.domain.model.Tag
import com.fila.domain.model.Task
import com.fila.domain.toDate
import com.fila.domain.toLocalDate

class TaskDbMapper(private val tagDbMapper: TagDbMapper): RepositoryMapper<Task, TaskEntity> {
    fun map(s: TaskEntity, tagsList: List<TagEntity>): Task {
        return Task(
                name = s.name,
                id = s.id,
                comment = s.comment,
                creationTime = s.creationTime.toLocalDate(),
                priority = fromOrdinal(s.priority),
                dueToDate = s.dueDate?.toLocalDate(),
                notificationEnabled = s.notificationEnabled,
                taskListId = s.listId,
                tags = tagsList.map(tagDbMapper::map)
        )
    }

    override fun mapToRepository(t: Task): TaskEntity {
        return TaskEntity(
                name = t.name,
                id = t.id,
                listId = t.taskListId,
                creationTime = t.creationTime.toDate(),
                comment = t.comment,
                priority = t.priority.ordinal,
                notificationEnabled = t.notificationEnabled,
                dueDate = t.dueToDate?.toDate(),
                tagIdList = t.tags?.map { it.id }
        )
    }
}