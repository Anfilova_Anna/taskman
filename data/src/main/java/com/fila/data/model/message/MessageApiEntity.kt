package com.fila.data.model.message

import com.google.gson.annotations.SerializedName

data class MessageApiEntity(
        @SerializedName("userName") val userName: String,
        @SerializedName("message") val message: String,
        @SerializedName("topic") val topic: String,
        @SerializedName("instanceId") val instanceId: String,
        @SerializedName("isCurrentUserMessage") val isCurrentUserMessage: Boolean)