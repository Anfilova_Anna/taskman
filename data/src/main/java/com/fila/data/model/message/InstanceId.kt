package com.fila.data.model.message

import com.google.gson.annotations.SerializedName

data class InstanceId(@SerializedName("instanceId") val instanceId: String)