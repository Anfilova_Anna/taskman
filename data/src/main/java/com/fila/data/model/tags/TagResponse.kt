package com.fila.data.model.tags

data class TagResponse( val name: String,
                val id: Int,
                val info: String? = null,
                val iconId: Int? = null)