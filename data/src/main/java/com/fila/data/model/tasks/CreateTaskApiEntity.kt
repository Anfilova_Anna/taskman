package com.fila.data.model.tasks

import com.fila.data.model.tags.TagResponse
import com.google.gson.annotations.SerializedName
import java.util.*

data class CreateTaskApiEntity(
        @SerializedName("name") val name: String,
        @SerializedName("creationTime") val creationTime: Date,
        @SerializedName("comment") val comment: String?,
        @SerializedName("priority") val priority: Int,
        @SerializedName("dueDate") val dueToDate: Date? = null,
        @SerializedName("notificationEnabled") val notificationEnabled: Boolean = false,
        @SerializedName("tags") val tags: List<TagResponse> = emptyList())
