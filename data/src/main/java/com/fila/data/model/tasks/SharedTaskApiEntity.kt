package com.fila.data.model.tasks

import com.google.gson.annotations.SerializedName

data class SharedTaskApiEntity(
        @SerializedName("userId") val userId: String,
        @SerializedName("taskId") val taskId: Long
)