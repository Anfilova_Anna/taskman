package com.fila.data.model.tasks

import com.fila.data.model.tags.TagResponse
import com.google.gson.annotations.SerializedName
import java.util.*

class TaskResponse(
        @SerializedName("name") val name: String,
        @SerializedName("id") val id: Long,
        @SerializedName("listId") val listId: Long,
        @SerializedName("creationTime") val creationTime: Date,
        @SerializedName("comment") val comment: String?,
        @SerializedName("priority") val priority: Int,
        @SerializedName("dueDate") val dueToDate: Date? = null,
        @SerializedName("notificationEnabled") val notificationEnabled: Boolean = false,
        @SerializedName("tags") val tags: List<TagResponse> = emptyList())