package com.fila.data.repository

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.channels.consumeEach
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

abstract class BaseRepository : CoroutineScope {

    override val coroutineContext: CoroutineContext = Job() + Dispatchers.IO

    @Suppress("EXPERIMENTAL_API_USAGE")
    private val actor = actor<JobDefinition<*>>(capacity = Channel.UNLIMITED) {
        consumeEach { launch(it.context) { it.block() }.join() }
    }

    protected suspend fun <TResult> execute(block: suspend () -> TResult): TResult =
            CompletableDeferred<TResult>().also { deferred ->
                actor.offer(JobDefinition(coroutineContext) { deferred.doTry { block() } })
            }.await()

    private suspend fun <T> CompletableDeferred<T>.doTry(block: suspend () -> T) {
        try {
            complete(block())
        } catch (ex: Throwable) {
            if (ex is CancellationException) {
                Timber.d(ex, "Completable was canceled normally because of parent context is cancelled.")
            } else {
                Timber.e(ex)
                completeExceptionally(ex)
            }
        }
    }

    class JobDefinition<T>(val context: CoroutineContext, val block: suspend () -> T)
}