package com.fila.data.repository

import com.fila.data.Api
import com.fila.data.db.TaskManDb
import com.fila.data.dbmapper.TaskDbMapper
import com.fila.data.mapper.TaskMapper
import com.fila.domain.model.Task
import com.fila.domain.repository.TaskRepository
import org.threeten.bp.LocalDate

class TaskRepositoryImpl(private val api: Api,
                         private val db: TaskManDb,
                         private val taskMapper: TaskMapper,
                         private val dbTaskMapper: TaskDbMapper) : BaseRepository(), TaskRepository {

    override suspend fun getTasks(): List<Task> = execute {

        db.taskDao().getAllTasks().map {
            dbTaskMapper.map(it, it.tagIdList?.let {db.tagDao().getAllTagsById(it) } ?: emptyList())
        }
    }

    override suspend fun getRemoteTasks(): List<Task> = execute {
        listOf(
                Task(name = "Task 1", id = 1, creationTime = LocalDate.now().minusDays(2), taskListId = 0L),
                Task(name = "Task 2", id = 2, creationTime = LocalDate.now().minusDays(1), taskListId = 0L),
                Task(name = "Task 3", id = 3, creationTime = LocalDate.now().minusDays(3), taskListId = 0L),
                Task(name = "Task 4", id = 4, creationTime = LocalDate.now().minusDays(4), taskListId = 0L)
        ).also {
            db.taskDao().insertTasks(it.map(dbTaskMapper::mapToRepository))
        }
//        api.getTasks().await().map (taskMapper::map).also { db.taskDao().insertTasks(it.map(dbTaskMapper::mapToRepository)) }
    }
}