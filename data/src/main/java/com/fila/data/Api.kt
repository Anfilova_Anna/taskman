package com.fila.data

import com.fila.data.model.message.InstanceId
import com.fila.data.model.message.MessageApiEntity
import com.fila.data.model.tasks.CreateTaskApiEntity
import com.fila.data.model.tasks.SharedTaskApiEntity
import com.fila.data.model.tasks.TaskResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

class Api(private val taskApi: TaskApi,
          private val messageApi: MessageApi) : TaskApi by taskApi, MessageApi by messageApi

interface MessageApi {

    @POST("/users/register")
    fun sendNewInstanceId(@Body var1: InstanceId): Deferred<String>

    @POST("/users/unsubscribe")
    fun unsubscribeInstanceId(@Body var1: InstanceId): Deferred<String>

    @POST("/users/message")
    fun sendMessage(@Body var1: MessageApiEntity): Deferred<String>
}

interface TaskApi {
    @GET("tasks/all")
    fun getTasks(): Deferred<List<TaskResponse>>

    @POST("tasks/add")
    fun addTask(@Body createTaskEApintity: CreateTaskApiEntity): Deferred<List<TaskResponse>>

    @POST("/tasks/shareTask")
    fun shareWithUser(@Body sharedTask: SharedTaskApiEntity): Deferred<String>
}
