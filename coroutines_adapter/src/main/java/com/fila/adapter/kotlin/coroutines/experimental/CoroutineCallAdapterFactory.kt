package com.fila.adapter.kotlin.coroutines.experimental

import com.google.gson.Gson
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import okhttp3.ResponseBody
import retrofit2.*
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.net.HttpURLConnection

class CoroutineCallAdapterFactory(val gson: Gson) : CallAdapter.Factory() {
    companion object {
        @JvmStatic
        @JvmName("create")
        operator fun invoke(gson: Gson) = CoroutineCallAdapterFactory(gson)
    }

    override fun get(
            returnType: Type,
            annotations: Array<out Annotation>,
            retrofit: Retrofit
    ): CallAdapter<*, *>? {
        if (Deferred::class.java != getRawType(returnType)) {
            return null
        }
        if (returnType !is ParameterizedType) {
            throw IllegalStateException(
                    "Deferred return type must be parameterized as Deferred<Foo> or Deferred<out Foo>")
        }
        val responseType = getParameterUpperBound(0, returnType)

        val rawDeferredType = getRawType(responseType)
        return if (rawDeferredType == Response::class.java) {
            if (responseType !is ParameterizedType) {
                throw IllegalStateException(
                        "Response must be parameterized as Response<Foo> or Response<out Foo>")
            }
            ResponseCallAdapter<Any>(getParameterUpperBound(0, responseType))
        } else {
            BodyCallAdapter<Any>(responseType)
        }
    }

    private inner class BodyCallAdapter<T>(
            private val responseType: Type
    ) : CallAdapter<T, Deferred<T>> {

        override fun responseType() = responseType

        override fun adapt(call: Call<T>): Deferred<T> {
            val deferred = CompletableDeferred<T>()

            deferred.invokeOnCompletion {
                if (deferred.isCancelled) {
                    call.cancel()
                }
            }

            call.enqueue(object : Callback<T> {
                override fun onFailure(call: Call<T>, t: Throwable) {
                    deferred.completeExceptionally(t)
                }

                override fun onResponse(call: Call<T>, response: Response<T>) {
                    if (response.isSuccessful) {
                        //Changes START
                        response.body()
                                ?.let { deferred.complete(it) }
                                ?: deferred.completeExceptionally(NullBodyException)
                        //Changes END
                    } else {
                        handleFailedResponse(deferred, response)
                    }
                }
            })

            return deferred
        }

        private fun handleFailedResponse(deferred: CompletableDeferred<T>, response: Response<T>) {
            if (response.code() >= HttpURLConnection.HTTP_MULT_CHOICE) {
                try {
                    val exception = response.errorBody()
                            ?.let { it as? ResponseBody }
                            ?.let {
                                gson.fromJson(it.charStream(), RetrofitError::class.java).toRetrofitException(response.code())
                            } ?: HttpException(response)
                    deferred.completeExceptionally(exception)
                } catch (e: Throwable) {
                    deferred.completeExceptionally(HttpException(response))
                }
            } else {
                deferred.completeExceptionally(HttpException(response))
            }
        }
    }

    private class ResponseCallAdapter<T>(
            private val responseType: Type
    ) : CallAdapter<T, Deferred<Response<T>>> {

        override fun responseType() = responseType

        override fun adapt(call: Call<T>): Deferred<Response<T>> {
            val deferred = CompletableDeferred<Response<T>>()

            deferred.invokeOnCompletion {
                if (deferred.isCancelled) {
                    call.cancel()
                }
            }

            call.enqueue(object : Callback<T> {
                override fun onFailure(call: Call<T>, t: Throwable) {
                    deferred.completeExceptionally(t)
                }

                override fun onResponse(call: Call<T>, response: Response<T>) {
                    deferred.complete(response)
                }
            })

            return deferred
        }
    }
}
