package com.fila.adapter.kotlin.coroutines.experimental

class RetrofitException(
        override val message: String,
        val responseCode: Int,
        val reasonCode: Int?
) : Exception()