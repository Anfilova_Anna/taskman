package com.fila.adapter.kotlin.coroutines.experimental

import com.google.gson.annotations.SerializedName

class RetrofitError(
        @SerializedName("message")
        val message: String,
        @SerializedName("reasonCode")
        val reasonCode: Int?
) {
    fun toRetrofitException(responseCode: Int): RetrofitException =
            RetrofitException(message, responseCode, reasonCode)
}