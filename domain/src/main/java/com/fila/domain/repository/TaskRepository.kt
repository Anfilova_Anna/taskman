package com.fila.domain.repository

import com.fila.domain.model.Task

interface TaskRepository {
    suspend fun getTasks() : List<Task>
    suspend fun getRemoteTasks() : List<Task>
}