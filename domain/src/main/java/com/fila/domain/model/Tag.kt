package com.fila.domain.model

data class Tag(
        val name: String,
        val id: Int,
        val info: String? = null,
        val iconId: Int? = null)