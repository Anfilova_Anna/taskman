package com.fila.domain.model

import org.threeten.bp.LocalDate

data class Task(val name: String,
                val id: Long,
                val taskListId: Long,
                val creationTime: LocalDate,
                val comment: String? = null,
                val priority: Priority = Priority.DEFAULT,
                val dueToDate: LocalDate? = null,
                val notificationEnabled: Boolean = false,
                val tags: List<Tag>? = null) {

    enum class Priority {
        EXTREMELY_HIGH, HIGH, MEDIUM, LOW;

        companion object {
            val DEFAULT = MEDIUM
        }
    }
}