package com.fila.domain

import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consume
import org.threeten.bp.*
import org.threeten.bp.temporal.ChronoUnit
import java.io.File
import java.util.*

fun String?.appendText(additionalText: String, separator: Char = ','): String =
        when {
            this == null -> additionalText
            additionalText.isNotBlank() && isNotBlank() && last() != separator -> "$this$separator$additionalText"
            else -> "$this$additionalText"
        }

suspend fun <E> ReceiveChannel<E>.firstOrDefault(default: E): E =
        consume {
            val iterator = iterator()
            return if (!iterator.hasNext()) default
            else iterator.next()
        }

fun LocalDate.toEpochMilli(endOfDay: Boolean = false) =
        if (endOfDay) {
            atTime(LocalTime.MAX)
        } else {
            atTime(LocalTime.MIN)
        }.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()

fun LocalDateTime.toEpochMilli() = atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()

fun LocalDate.toDate(endOfDay: Boolean = false) = Date(this.toEpochMilli(endOfDay))

fun Instant.toLocalDate(): LocalDate = atZone(ZoneId.systemDefault()).toLocalDate()

fun Instant.toLocalDateTime(): LocalDateTime = atZone(ZoneId.systemDefault()).toLocalDateTime()

fun Date.diffInHours(date: Date) = DateTimeUtils.toInstant(this).toLocalDateTime().let {
    ChronoUnit.HOURS.between(it, DateTimeUtils.toInstant(date).toLocalDateTime())
}

fun Date.toLocalDate(): LocalDate = DateTimeUtils.toInstant(this).toLocalDate()

fun Collection<*>?.isNullOrEmpty() = this?.run { size == 0 } ?: true

fun Collection<*>?.isNotNullOrEmpty() = this?.run { size != 0 } ?: false

inline fun <T : Any> MutableList<T>.addOrReplace(item: T, comparatorBlock: (T, T) -> Boolean = { a, b -> a == b }): Boolean {

    val indexToUpdate = indexOfFirst { comparatorBlock(it, item) }

    return if (indexToUpdate == -1) add(item).let { false }
    else set(indexToUpdate, item).let { true }
}

fun <T : Any> MutableList<T>.replaceElementIf(item: T, predicate: (T) -> Boolean = { a -> a == item }): Boolean {
    val indexToUpdate = indexOfFirst { predicate(it) }
    return if (indexToUpdate != -1) set(indexToUpdate, item).let { true }
    else false
}

inline fun <T : Any, K> MutableList<T>.replaceElement(item: T, keyExtractor: (T) -> K): Boolean {
    val itemKey = keyExtractor(item)
    val indexToUpdate = indexOfFirst { keyExtractor(it) == itemKey }
    return if (indexToUpdate != -1) set(indexToUpdate, item).let { true }
    else false
}

inline fun <T : Any> MutableList<T>.removeElementIf(predicate: (T) -> Boolean): Boolean {
    val indexToRemove = indexOfFirst { predicate(it) }
    return if (indexToRemove != -1) removeAt(indexToRemove).let { true }
    else false
}

inline fun <T : Any> MutableList<T>.removeElementWithIndexIf(predicate: (T) -> Boolean): Int {
    val indexToRemove = indexOfFirst { predicate(it) }
    return if (indexToRemove != -1) removeAt(indexToRemove).let { indexToRemove }
    else indexToRemove
}

inline fun <T : Any, K> MutableList<T>.removeAllBy(items: Iterable<T>, keyExtractor: (T) -> K) {
    for (item in items) {
        val itemKey = keyExtractor(item)
        removeElementIf { keyExtractor(it) == itemKey }
    }
}

inline fun <T : Any, K> MutableList<T>.removeAllBy(items: Array<out T>, keyExtractor: (T) -> K) {
    for (item in items) {
        val itemKey = keyExtractor(item)
        removeElementIf { keyExtractor(it) == itemKey }
    }
}

inline fun <T : Any, K : Any> MutableList<T>.addAllUnique(other: Iterable<T>, keyExtractor: (T) -> K) {
    val result = mutableListOf<T>()
    other.forEach { itemToCheck ->
        val key = keyExtractor(itemToCheck)
        if (any { keyExtractor(it) == key }) return@forEach
        else result.add(itemToCheck)
    }
    addAll(result)
}

inline fun <T> tryOrNull(block: () -> T): T? =
        try {
            block()
        } catch (ex: Throwable) {
            null
        }

fun <T : Comparable<T>> SortedSet<T>.suggestedPositionToInsertion(t: T) = when {
    this.isEmpty() -> null
    t < first() -> -1 // As first element
    t >= last() -> 1 // As last element
    else -> 0 // In the middle of array
}

tailrec fun <T> Collection<(T) -> T>.mutate(initialData: T): T =
        if (size == 1) first()(initialData)
        else drop(1).mutate(first()(initialData))

inline fun <T : Any> List<T>.split(byCount: Int): List<List<T>> {
    if (size < byCount) return listOf(this)
    val batchCount = size / byCount + if (size % byCount > 0) 1 else 0
    val result = mutableListOf<List<T>>()
    for (i in 0 until batchCount) {
        val startIndex = i * byCount
        val bound = (i + 1) * byCount
        val endIndex = if (bound < size) bound else size
        result.add(subList(startIndex, endIndex))
    }
    return result
}

infix operator fun File.plus(additionalPath: String) = path + File.separator + additionalPath

inline fun <reified T : Enum<T>> fromOrdinal(ordinal: Int): T {
    for (element in enumValues<T>()) {
        if (element.ordinal == ordinal) return element
    }
    throw IllegalArgumentException()
}