package com.fila.domain.common

import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.consumeEach

private abstract class Debounce<T>(
        private val timeout: Long,
        private val parentChannel: ReceiveChannel<T>,
        private val delegateChannel: Channel<T>) : Channel<T> by delegateChannel {

    var consumingJob: Job? = null
    val context = Dispatchers.Default + Job()

    init {
        GlobalScope.launch(context) {
            parentChannel.consumeEach {
                consumingJob.takeIf { isActive }?.cancel()
                consumingJob = launch(context) {
                    delay(timeout)
                    send(it)
                }
            }
        }
    }

    override fun cancel() {
        context.cancel()
        delegateChannel.cancel()
        parentChannel.cancel()
    }
}

private class ConflatedDebounce<T>(timeout: Long, channel: ReceiveChannel<T>)
    : Debounce<T>(timeout, channel, Channel<T>(Channel.CONFLATED))

fun <T> ReceiveChannel<T>.debounce(timeout: Long): ReceiveChannel<T> =
        ConflatedDebounce(timeout, this)