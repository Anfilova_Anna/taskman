package com.fila.domain.interactor

import com.fila.domain.model.Task
import com.fila.domain.repository.TaskRepository

class TaskInteractor(private val taskRepositoty: TaskRepository) {

    suspend fun getTasks(): List<Task> {
        return taskRepositoty.getRemoteTasks()
    }
}