package com.fila.taskman

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.fila.data.Api
import com.fila.data.model.message.InstanceId
import com.fila.data.model.message.MessageApiEntity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.demo.fragment_people.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.android.ext.android.get
import timber.log.Timber


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PeopleFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [PeopleFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class PeopleFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var token: String = ""
    private var param1: String? = null
    private var param2: String? = null
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_people, container, false)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
//        if (context is OnFragmentInteractionListener) {
//            listener = context
//        } else {
//            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
//        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        FirebaseInstanceId.getInstance().instanceId
                .addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Timber.e(task.exception, "getInstanceId failed")
                        return@OnCompleteListener
                    }

                    // Get new Instance ID token
                    token = task.result?.token ?: ""
                    // Log and toast
                    val msg = "new token $token"
                    instanceID.setText(token)
                    Timber.w("new token")
                    Timber.w(msg)
                    Toast.makeText(activity!!, msg, Toast.LENGTH_SHORT).show()
                })

        btnSend.setOnClickListener {
            GlobalScope.launch {
                val api: Api = get()
                api.sendMessage(MessageApiEntity(userName = userName.text.toString(), message = message.text.toString(), topic = topic.text.toString(), instanceId = token, isCurrentUserMessage = true)).await().let {
                    Timber.w("after send: $it")
                    launch(Dispatchers.Main) { Toast.makeText(activity, it, Toast.LENGTH_SHORT).show() }
                }

            }
//
//            val remoteMessage = RemoteMessage.Builder("Anna")
//                    .setMessageId(Integer.toString(1))
//                    .addData("message", "Hello World")
//                    .addData("topic","sayHello")
//                    .build()
//            FirebaseMessaging.getInstance().send(remoteMessage)
        }

        btnRegister.setOnClickListener {
            GlobalScope.launch {
                val api: Api = get()
                api.sendNewInstanceId(InstanceId(instanceId = token)).await().let {
                    Timber.w("after register: $it")
                    launch(Dispatchers.Main) { Toast.makeText(activity, it, Toast.LENGTH_SHORT).show() }
                }
            }
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments]
     * (http://developer.android.com/training/basics/fragments/communicating.html)
     * for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PeopleFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
                PeopleFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_PARAM1, param1)
                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
