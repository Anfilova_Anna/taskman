package com.fila.taskman.presentation.tasks

import android.app.Activity
import android.app.AlertDialog
import android.os.Bundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupWithNavController
import com.arellomobile.mvp.MvpAppCompatActivity
import com.fila.taskman.R
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber

class TaskMainActivity : MvpAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        bottomNavigation.setupWithNavController(findNavController(R.id.host_fragment))

        val navController = Navigation.findNavController(this, R.id.host_fragment)
        navController.addOnDestinationChangedListener { navController, destination, arguments ->
            Timber.d("New destination: " + destination.label.toString())
        }

        val appBarConfiguration = AppBarConfiguration(setOf(R.id.mainTaskFragment, R.id.settingFragment, R.id.peopleFragment))
        toolbar.setupWithNavController(navController, appBarConfiguration)

    }

    override fun onResume() {
        super.onResume()

        if (checkGooglePlayServicesAvailability(activity = this, cancellationBlock = { finish() })) {
            // do work
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return menuInflater.inflate(R.menu.setting_menu, menu).let { true }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(findNavController(R.id.host_fragment))
                || super.onOptionsItemSelected(item)
    }

    private fun checkGooglePlayServicesAvailability(activity: Activity, requstCode: Int = 8542, cancellationBlock: () -> Unit): Boolean {
        val googleApiAvailability = GoogleApiAvailability.getInstance()
        val status = googleApiAvailability.isGooglePlayServicesAvailable(activity)
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, requstCode) { cancellationBlock() }.apply {
                    setOnShowListener {
                        it as AlertDialog
                        it.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
                            dismiss()
                            cancellationBlock()
                        }
                    }
                    show()
                }
            }
            return false
        }
        return true
    }
}