package com.fila.taskman.presentation.tasks

import com.fila.domain.model.Task
import com.fila.taskman.presentation.base.BaseView

interface MainTaskView : BaseView {
    fun showMessage(message: Int)
    fun showTasks(tasks: List<Task>)
    fun addNewOne(listId: Long)
}