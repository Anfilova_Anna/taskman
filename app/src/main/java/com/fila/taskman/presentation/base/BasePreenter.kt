package com.fila.taskman.presentation.base

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.SendChannel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.channels.consumeEach
import org.koin.standalone.KoinComponent
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

abstract class BasePresenter<TView : MvpView, TEvent> : MvpPresenter<TView>(), KoinComponent, CoroutineScope {

    private val job: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Default /* + Dispatchers.logExceptionHandler(this::class.java.name) */

    /**
     * Channel where UI events are passed.
     */
    val eventChannel = actor<Any>(capacity = Channel.UNLIMITED) {
        consumeEach { event ->
            try {
                (event as TEvent)?.let {
                    Timber.d("[${this@BasePresenter.javaClass.simpleName}]:[Event] = $it")
                    handleEvent(it)
                }
            } catch (e: ClassCastException) {
                Timber.e(e, "Unknown event.")
            }
        }
    }

    /**
     * Channel that is intended to handle long running operations, such as calls to interactors.
     */
    @ObsoleteCoroutinesApi
    private val longRunningChannel: SendChannel<suspend () -> Unit> =
            actor(capacity = Channel.UNLIMITED) { consumeEach { it() } }

    /**
     * Executes inside separate coroutine, defined by [BasePresenter.longRunningChannel].
     * Intended to be temporary solution to resolve multiple UI events handling simultaneously
     * (e.g. loading + navigating). Intended to be used with long running operations such as interactor usages.
     */
    protected fun goLongRunning(block: suspend () -> Unit) {
        @Suppress("EXPERIMENTAL_API_USAGE")
        if (longRunningChannel.isClosedForSend)
            Timber.e("[${javaClass.simpleName}]Trying to go long running on cleared presenter.")
        else
            longRunningChannel.offer(block)
    }

    protected suspend fun sendToView(block: () -> Unit) {
        withContext(Dispatchers.Main) {
            block()
        }
    }

    /**
     * Each [event] from [eventChannel] is passed to this
     * callback.
     */
    protected abstract suspend fun handleEvent(event: TEvent)

}