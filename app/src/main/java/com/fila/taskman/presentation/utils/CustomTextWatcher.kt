package com.fila.taskman.presentation.utils

import android.text.Editable
import android.text.TextWatcher

open class CustomTextWatcher (
        val beforeTextChanded: ((s: CharSequence?, start: Int, count: Int, after: Int) -> Any?)? = null,
        val afterTextChanged: ((s: Editable?) -> Any?)? = null,
        val textChanged: ((s: CharSequence?, start: Int, before: Int, count: Int) -> Any?)? = null
) : TextWatcher {

    override fun afterTextChanged(s: Editable?) {
        afterTextChanged?.invoke(s)
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        beforeTextChanded?.invoke(s, start, count, after)
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        textChanged?.invoke(s.toString(), start, before, count)
    }
}