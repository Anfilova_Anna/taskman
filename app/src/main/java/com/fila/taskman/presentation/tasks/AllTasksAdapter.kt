package com.fila.taskman.presentation.tasks

import android.content.Context
import android.support.design.chip.Chip
import android.support.design.chip.ChipGroup
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.fila.domain.model.Task
import com.fila.taskman.R
import com.fila.taskman.ext.setVisibility
import com.fila.taskman.presentation.base.RecyclerAdapter
import kotlinx.android.synthetic.main.list_item_task.view.*

class AllTasksAdapter(val context: Context) : RecyclerAdapter<Task, AllTasksAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item_task, parent, false))

    override fun onBindViewHolder(view: ViewHolder, position: Int) {
        val item = data[position]
        view.itemName.text = item.name
        view.commentView.text = item.comment
        view.priorityView.setBackgroundColor(ContextCompat.getColor(context, when (item.priority) {
            Task.Priority.EXTREMELY_HIGH -> R.color.priority_extremely_high
            Task.Priority.HIGH -> R.color.priority_high
            Task.Priority.MEDIUM -> R.color.priority_medium
            Task.Priority.LOW -> R.color.priority_low
        }))
        item.tags?.forEach {
            val chip = Chip(context)
            chip.text = it.name
//            chip.chipIcon = ContextCompat.getDrawable(requireContext(), baseline_person_black_18)
            chip.isCloseIconEnabled = true
            view.chipGroup.addView(chip)
        } ?: view.chipGroup.removeAllViews()
        view.notificationTimeView.setVisibility(item.notificationEnabled)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val itemName: TextView = view.taskNameView
        val commentView: TextView = view.сommentView
        val completeCheckBox: CheckBox = view.completeCheckBox
        val priorityView: View = view.priorityView
        val tagListView: TextView = view.notificationTimeView
        val chipGroup: ChipGroup = view.chipGroup
        val notificationTimeView: TextView = view.notificationTimeView
    }
}