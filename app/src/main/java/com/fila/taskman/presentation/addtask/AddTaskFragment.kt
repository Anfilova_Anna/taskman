package com.fila.taskman.presentation.addtask

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.fila.domain.model.Task
import com.fila.taskman.R
import com.fila.taskman.presentation.addtask.AddTaskView.Event
import com.fila.taskman.presentation.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_add_task.*
import org.threeten.bp.LocalDate

class AddTaskFragment : BaseFragment<AddTaskView, AddTaskView.Event, AddTaskPresenter>(), AddTaskView {

    @InjectPresenter
    override lateinit var presenter: AddTaskPresenter

    // TODO how to add some fragment with root and not root type

    private val listId: Long by lazy { AddTaskFragmentArgs.fromBundle(arguments).listId }

    @ProvidePresenter
    fun provideDetailsPresenter(): AddTaskPresenter {
        return AddTaskPresenter(listId)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_add_task, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnCreate.setOnClickListener {
            val task = CreateTaskInfo(
                    name = taskNameView.value,
                    comment = commentView.text.toString(),
                    priority = Task.Priority.DEFAULT,
                    notificationEnabled = notificationSwitch.isChecked,
                    dueToDate = LocalDate.now(), // TODO
                    taskListId = listId,
                    tags = null
            )
            sendEvent(Event.Create(task))
        }
    }


}