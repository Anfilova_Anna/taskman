package com.fila.taskman.presentation.base

import android.support.annotation.ColorInt
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.TypedValue
import android.widget.TextView
import com.fila.taskman.R
import com.fila.domain.common.debounce
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.actor
import kotlinx.coroutines.channels.consumeEach
import timber.log.Timber

class SnackbarWrapper(
        private val activity: AppCompatActivity,
        coroutineScope: CoroutineScope = GlobalScope,
        @ColorInt private var backgroundColor: Int = 0) {

    private companion object {
        const val DELAY_BETWEEN_EVENTS = 100L
        const val SNACK_BAR_MAX_LINES = 10
        const val LINE_SPACING_EXTRA = 8f
        const val TEXT_SIZE = 16f
    }

    private var text: String = ""
    private var snackbar: Snackbar? = null

    private val actor = coroutineScope.actor<String>(capacity = Channel.UNLIMITED) {
        debounce(DELAY_BETWEEN_EVENTS).consumeEach {
            it.takeIf { it != text }?.let {
                if (it.isEmpty()) {
                    dismissSnackbar()
                } else {
                    text = it
                    snackbar?.dismiss()
                    snackbar = createSnackbar(text).apply { show() }
                }
            }
        }
    }.apply { invokeOnClose { snackbar?.dismiss() } }

    var onOkClick: (() -> Any)? = null

    fun setErrorText(text: String) {
        if (actor.isClosedForSend) Timber.w("Trying to send message [$text] to closed channel")
        else actor.offer(text)
    }

    fun setErrorText(@StringRes text: Int) = setErrorText(activity.getString(text))

    private fun createSnackbar(text: String): Snackbar = Snackbar
            .make(activity.findViewById(android.R.id.content),
                    "",
                    Snackbar.LENGTH_INDEFINITE)
            .setAction(activity.getString(android.R.string.ok)) {
                dismissSnackbar()
                onOkClick?.invoke()
            }
            .setText(text)
            .apply { setSnackbarBackground() }
            .apply { setupTextStyle() }

    private fun dismissSnackbar() {
        text = ""
        snackbar?.let {
            it.dismiss()
            snackbar = null
        }
    }

    private fun Snackbar.setSnackbarBackground() {
        view.setBackgroundColor(
                if (backgroundColor != 0) backgroundColor
                else ContextCompat.getColor(context, R.color.snackBarColor)
        )
    }

    private fun Snackbar.setupTextStyle() {
        val snackbarText = view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        val spacing = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, LINE_SPACING_EXTRA, context.resources.displayMetrics)

        snackbarText.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_SIZE)
        snackbarText.maxLines = SNACK_BAR_MAX_LINES
        snackbarText.setLineSpacing(spacing, 1f)
    }
}