package com.fila.taskman.presentation.widgets

interface FilterWidget {
    fun clear()
    fun setChangedCallback(callback: ((FilterWidget) -> Unit)?)
    fun isChanged(): Boolean
}