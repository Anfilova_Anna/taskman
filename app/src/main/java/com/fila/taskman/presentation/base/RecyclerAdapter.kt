package com.fila.taskman.presentation.base

import android.support.v7.widget.RecyclerView

abstract class RecyclerAdapter<Model, ViewHolder : RecyclerView.ViewHolder> : RecyclerView.Adapter<ViewHolder>() {

    protected var data: List<Model> = emptyList()

    override fun getItemCount() = data.size

    open fun setListData(list: List<Model>): Boolean {
        if (data == list) return false

        data = list
        notifyDataSetChanged()
        return true
    }
}