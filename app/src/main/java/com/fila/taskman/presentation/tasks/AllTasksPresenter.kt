package com.fila.taskman.presentation.tasks

import com.arellomobile.mvp.InjectViewState
import com.fila.domain.interactor.TaskInteractor
import com.fila.taskman.presentation.base.BasePresenter
import com.fila.taskman.presentation.tasks.AllTasksPresenter.Event
import org.koin.standalone.get
import org.koin.standalone.inject
import timber.log.Timber

@InjectViewState
class AllTasksPresenter : BasePresenter<MainTaskView, Event>() {

    private val taskInteractor: TaskInteractor by inject()
    private var listId: Long = 0  // TODO get it from elsewhere (make the menu for choose the list)

    override suspend fun handleEvent(event: Event) {
        when (event) {
            Event.AddNewOne -> handleAddNewOne()
            Event.GetTasks -> goLongRunning { getTasks() }
        }
    }

    sealed class Event {
        object GetTasks : Event()
        object AddNewOne : Event()
    }

    private suspend fun getTasks() {
        try {
            val list = taskInteractor.getTasks()
            Timber.d("list size: ${list.size}")
            sendToView { viewState.showTasks(list) }
        } catch (e: Exception) {
            Timber.e("exception")
        }
    }

    private suspend fun handleAddNewOne() {
        sendToView {
            viewState.addNewOne(listId = listId)
        }
    }
}