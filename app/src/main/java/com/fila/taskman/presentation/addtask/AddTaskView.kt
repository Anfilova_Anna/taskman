package com.fila.taskman.presentation.addtask

import com.fila.taskman.presentation.base.BaseView

interface AddTaskView : BaseView {

    sealed class Event {
        class Create(task: CreateTaskInfo): Event()
    }
}