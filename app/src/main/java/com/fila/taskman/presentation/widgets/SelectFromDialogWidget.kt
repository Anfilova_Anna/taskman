package com.fila.taskman.presentation.widgets

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v4.content.ContextCompat
import android.text.InputType
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.fila.taskman.R
import com.fila.taskman.ext.onTextChanged

private const val DRAWABLE_PADDING_FACTOR = 2

open class SelectFromDialogWidget @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0)
    : TextInputLayout(context, attrs, defStyleAttr), FilterWidget {

    private companion object {
        @JvmField
        val SUPER_STATE_KEY = SelectFromDialogWidget::class.java.name + ".state.SUPER"
        @JvmField
        val VALUE_KEY = SelectFromDialogWidget::class.java.name + ".state.VALUE"
    }

    private val editText = TextInputEditText(context)

    var defaultValue: String? = null
        set(value) {
            field = value
            this.value = value
        }

    var value: String? = null
        set(value) {
            field = value
            editText.setText(value ?: "")

            setButtonDrawable()
        }

    var onArrowClickListener: ((view: View) -> Unit)? = null
    var onClearListener: ((view: SelectFromDialogWidget) -> Unit)? = null

    init {
        editText.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context, R.color.silver))
        editText.textSize = resources.getDimension(R.dimen.font_sub_header) / resources.displayMetrics.scaledDensity
        editText.inputType = InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
        editText.setOnTouchListener { v, event ->
            event?.let {
                if (it.action == MotionEvent.ACTION_UP) {
                    if (isChanged()) {
                        if (event.x > v.width - v.height) {
                            clear()
                        } else {
                            onArrowClickListener?.invoke(this)
                        }
                    } else {
                        onArrowClickListener?.invoke(this)
                    }
                }
            }
            true
        }
        this.addView(editText)
        editText.compoundDrawablePadding = height / DRAWABLE_PADDING_FACTOR
        setButtonDrawable()
    }

    override fun onSaveInstanceState(): Parcelable = Bundle().apply {
        putParcelable(SUPER_STATE_KEY, super.onSaveInstanceState())
        putString(VALUE_KEY, value)
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state is Bundle) {
            super.onRestoreInstanceState(state.getParcelable(SUPER_STATE_KEY))
            value = state.getString(VALUE_KEY)
        } else {
            value = defaultValue
            super.onRestoreInstanceState(state)
        }
    }

    override fun clear() {
        value = defaultValue
        onClearListener?.invoke(this)
    }

    override fun setChangedCallback(callback: ((FilterWidget) -> Unit)?) {
        editText.onTextChanged {
            callback?.invoke(this)
        }
    }

    override fun isChanged(): Boolean = value != defaultValue

    private fun setButtonDrawable() {
        if (isChanged()) {
            R.drawable.ic_clear
        } else {
            R.drawable.ic_dropdown
        }.let {
            editText.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, it, 0)
        }
    }
}