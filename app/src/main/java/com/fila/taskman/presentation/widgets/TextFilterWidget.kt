package com.fila.taskman.presentation.widgets

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.os.Parcel
import android.os.Parcelable
import android.support.annotation.ColorRes
import android.support.annotation.IdRes
import android.support.design.widget.TextInputLayout
import android.support.v4.content.ContextCompat
import android.text.InputFilter
import android.text.InputType
import android.util.AttributeSet
import android.util.SparseArray
import android.view.View
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageView
import com.fila.domain.appendText
import com.fila.taskman.R
import com.fila.taskman.ext.*
import com.fila.taskman.presentation.utils.CustomTextWatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class TextFilterWidget(context: Context, attrs: AttributeSet?)
    : FrameLayout(context, attrs), FilterWidget {

    private companion object {
        const val TEXT_END_PADDING = R.dimen.titled_text_right_padding
    }

    private enum class WidgetInputType(val inputType: Int) {
        TEXT(InputType.TYPE_CLASS_TEXT),
        NUMBER(InputType.TYPE_CLASS_NUMBER),
        NONE(InputType.TYPE_NULL)
    }

    private val defaultText: String?
    private val filterText: EditText
    private val clear: ImageView
    private val additional: ImageView
    private val showKeyboardOnAttach: Boolean
    private val showAdditionalIcon: Boolean
    private val applyAdditionalPadding: Boolean
    private var changeCallback: ((FilterWidget) -> Unit)? = null

    val value: String
        get() = filterText.string()

    var onEditorActionClick: ((Int) -> Boolean)? = null

    var isClearHidden: Boolean = false
        set(value) {
            field = value
            clear.setVisibility(!value && filterText.string().isNotEmpty())
            updateFilterTextPadding()
        }

    var textColor: Int = ContextCompat.getColor(context, R.color.textColorDark)
        set(value) {
            field = value
            filterText.setTextColor(value)
        }

    init {

        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.TextFilterWidget, 0, 0)

        val isCustomLayoutDefined = a.hasValue(R.styleable.TextFilterWidget_customLayout)

        val layout = if (isCustomLayoutDefined) a.getResourceId(R.styleable.TextFilterWidget_customLayout, 0)
        else R.layout.text_filter_widget

        View.inflate(context, layout, this)

        val (filterTextId, clearId, additionalId) = getViewIds(a)

        filterText = findViewById(filterTextId)
        clear = findViewById(clearId)
        additional = findViewById(additionalId)

        defaultText =
                if (a.hasValue(R.styleable.TextFilterWidget_defaultValue)) a.getString(R.styleable.TextFilterWidget_defaultValue)
                else null

        showKeyboardOnAttach = a.getBoolean(R.styleable.TextFilterWidget_showKeyboardOnAttach, false)

        if (a.hasValue(R.styleable.TextFilterWidget_maxLength)) {
            a.getInt(R.styleable.TextFilterWidget_maxLength, 0).takeIf { it > 0 }
                    ?.let { InputFilter.LengthFilter(it) }
                    ?.let { filterText.filters.toMutableList().apply { add(it) } }
                    ?.let { filterText.filters = it.toTypedArray() }
        }

        if (a.hasValue(R.styleable.TextFilterWidget_hint)) {
            val hint = a.getString(R.styleable.TextFilterWidget_hint) ?: ""
            (findViewById<View>(R.id.inputLayout) as? TextInputLayout)
                    ?.let { it.hint = hint }
                    ?: let { filterText.hint = hint }
        }

        if (a.hasValue(R.styleable.TextFilterWidget_inputType)) {
            a.getInt(R.styleable.TextFilterWidget_inputType, WidgetInputType.TEXT.ordinal)
                    .let { WidgetInputType.values()[it] }
                    .let { filterText.inputType = it.inputType }
        }

        if (a.hasValue(R.styleable.TextFilterWidget_textColor)) {
            a.getColor(R.styleable.TextFilterWidget_textColor, ContextCompat.getColor(context, R.color.textColorDark))
                    .let { textColor = it }
        }

        isClearHidden = a.getBoolean(R.styleable.TextFilterWidget_clearHidden, false)

        showAdditionalIcon = if (a.hasValue(R.styleable.TextFilterWidget_additionalIcon)) {
            a.getResourceId(R.styleable.TextFilterWidget_additionalIcon, 0)
                    .takeIf { it != 0 }
                    ?.let {
                        additional.setVisibility(true)
                        additional.setImageResource(it)
                    }?.let { true } ?: false
        } else {
            additional.isVisible()
        }

        applyAdditionalPadding = !a.getBoolean(R.styleable.TextFilterWidget_notApplyIconsPadding, false)

        if (a.hasValue(R.styleable.TextFilterWidget_android_imeOptions)) {
            a.getInt(R.styleable.TextFilterWidget_android_imeOptions, 0)
                    .let { filterText.imeOptions = it }
        }

        a.getBoolean(R.styleable.TextFilterWidget_android_focusable, true)
                .let { filterText.isFocusable = it }

        a.getBoolean(R.styleable.TextFilterWidget_android_clickable, true)
                .let { filterText.isClickable = it }

        a.recycle()

        defaultText?.let { filterText.setText(it) }
        clear.setVisibility(!defaultText.isNullOrBlank() && !isClearHidden)
        clear.setOnClickListener { filterText.setText("") }
        updateFilterTextPadding()

        val listener: ((Boolean) -> Unit) = { isEmpty ->
            clear.setVisibility(!isClearHidden && !isEmpty)
            changeCallback?.invoke(this)
        }
//        if (filterText is MagicEditText) {
//            filterText.textChangedListener = { text ->
//                listener.invoke(text.isEmpty())
//            }
//        } else {
        filterText.addTextChangedListener(CustomTextWatcher { s, _, _, _ ->
            listener.invoke(s.isNullOrEmpty())
        })
//        }

        filterText.setOnEditorActionListener { _, actionId, _ ->
            onEditorActionClick?.invoke(actionId) ?: false
        }
    }

    fun addText(additionalText: String) {
        val text = filterText.string()

        text.appendText(additionalText)
                .also { filterText.setText(it) }
                .also { filterText.setSelection(it.length) }
    }

    fun setText(text: String, notifyListener: Boolean = true) {
        if (notifyListener) {
            filterText.setTextWithSelection(text)
        } else {
            val callback = changeCallback
            changeCallback = null
            filterText.setTextWithSelection(text)
            changeCallback = callback
        }
    }

    fun setSelection(position: Int) {
        filterText.setSelection(position)
    }

    fun setOnAdditionalClickListener(listener: () -> Unit) {
        additional.setOnClickListener { listener() }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (showKeyboardOnAttach) GlobalScope.launch(Dispatchers.Main) {
            filterText.showSoftKeyboard()
        }
    }

    override fun onDetachedFromWindow() {
        if (showKeyboardOnAttach) hideSoftKeyboard()
        super.onDetachedFromWindow()
    }

    override fun clear() {
        (defaultText ?: "")
                .also { filterText.setText(it) }.length
                .takeIf { it > 0 }
                ?.let { filterText.setSelection(it) }
    }

    override fun setChangedCallback(callback: ((FilterWidget) -> Unit)?) {
        changeCallback = callback
    }

    override fun isChanged(): Boolean = filterText.string().let {
        if (defaultText != null) it != defaultText
        else it.isNotEmpty()
    }

    override fun onSaveInstanceState(): Parcelable {
        val parentState = super.onSaveInstanceState()
        val state = ViewState(parentState)
        for (i in 0 until childCount) {
            getChildAt(i).saveHierarchyState(state.childrenState)
        }
        return state
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        state as ViewState
        super.onRestoreInstanceState(state.superState)
        for (i in 0 until childCount) {
            getChildAt(i).restoreHierarchyState(state.childrenState)
        }
    }

    override fun dispatchSaveInstanceState(container: SparseArray<Parcelable>?) {
        dispatchFreezeSelfOnly(container)
    }

    override fun dispatchRestoreInstanceState(container: SparseArray<Parcelable>?) {
        dispatchThawSelfOnly(container)
    }

    override fun setOnClickListener(listener: OnClickListener?) {
        super.setOnClickListener(listener)
        filterText.setOnClickListener(listener)
    }

    fun setImeOptions(imeOptions: Int) {
        filterText.imeOptions = imeOptions
    }

    fun setRawInputType(rawInputType: Int) {
        filterText.setRawInputType(rawInputType)
    }

    fun setIsEditEnabled(isEditEnabled: Boolean) {
        filterText.isEnabled = isEditEnabled
    }

    fun setIsEditFocusable(isEditFocusable: Boolean) {
        filterText.isFocusable = isEditFocusable
    }

    private fun updateFilterTextPadding() {
        if (applyAdditionalPadding) {
            val visibleImageViewsCount = when {
                !isClearHidden && showAdditionalIcon -> 2
                !isClearHidden || showAdditionalIcon -> 1
                else -> 0
            }
            val endPadding = context.resources.getDimension(TEXT_END_PADDING).let { visibleImageViewsCount * it }.toInt()
            filterText.setPadding(filterText.paddingStart, filterText.paddingTop, endPadding, filterText.paddingBottom)
        }
    }

    private fun getViewIds(a: TypedArray): Triple<Int, Int, Int> {
        @IdRes
        val filterTextId: Int
        @IdRes
        val clearId: Int
        @IdRes
        val additionalId: Int

        if (a.hasValue(R.styleable.TextFilterWidget_customLayout)) {
            filterTextId = if (a.hasValue(R.styleable.TextFilterWidget_editTextId))
                a.getResourceId(R.styleable.TextFilterWidget_editTextId, 0)
            else R.id.filterText

            clearId = if (a.hasValue(R.styleable.TextFilterWidget_clearId))
                a.getResourceId(R.styleable.TextFilterWidget_clearId, 0)
            else R.id.clear

            additionalId = if (a.hasValue(R.styleable.TextFilterWidget_additionalId))
                a.getResourceId(R.styleable.TextFilterWidget_additionalId, 0)
            else R.id.additional
        } else {
            filterTextId = R.id.filterText
            clearId = R.id.clear
            additionalId = R.id.additional
        }

        return Triple(filterTextId, clearId, additionalId)
    }

    class ViewState : BaseSavedState {

        var childrenState: SparseArray<Parcelable> = SparseArray()

        constructor(parcel: Parcel, classLoader: ClassLoader? = null) : super(parcel) {
            childrenState = parcel.readSparseArray(classLoader) as SparseArray<Parcelable>
        }

        constructor(state: Parcelable) : super(state)

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            super.writeToParcel(parcel, flags)
            parcel.writeSparseArray(childrenState as SparseArray<Any>)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.ClassLoaderCreator<ViewState> {
            override fun createFromParcel(source: Parcel, loader: ClassLoader): ViewState {
                return ViewState(source, loader)
            }

            override fun createFromParcel(parcel: Parcel): ViewState {
                return ViewState(parcel)
            }

            override fun newArray(size: Int): Array<ViewState?> {
                return arrayOfNulls(size)
            }
        }
    }

    fun setMaxTextLength(maxLength: Int) {
        filterText.setMaxTextLength(maxLength)
    }
}