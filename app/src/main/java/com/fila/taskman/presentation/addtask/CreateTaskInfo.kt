package com.fila.taskman.presentation.addtask

import com.fila.domain.model.Tag
import com.fila.domain.model.Task
import org.threeten.bp.LocalDate

data class CreateTaskInfo(
        val name: String,
        val taskListId: Long,
        val comment: String? = null,
        val priority: Task.Priority = Task.Priority.DEFAULT,
        val dueToDate: LocalDate? = null,
        val notificationEnabled: Boolean = false,
        val tags: List<Tag>? = null
)