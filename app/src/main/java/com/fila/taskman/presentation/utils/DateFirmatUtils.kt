package com.fila.taskman.presentation.utils

import android.annotation.SuppressLint
import org.threeten.bp.format.DateTimeFormatter
import java.text.SimpleDateFormat
import java.util.*

private const val LONG_YEAR = "dd.MM.yyyy"
private const val SHORT_YEAR = "dd.MM.yy"
private const val SHORT_DAY_MONTH = "dd.MM"
private const val LONG_YEAR_WITH_TIME = "dd.MM.yyyy HH:mm"
private const val SHORT_YEAR_WITH_TIME = "dd.MM.yy HH:mm"

private const val LONG_YEAR_WITH_SPACES = "dd MMMM yyyy"
private const val LONG_YEAR_WITH_TIME_AND_SPACES = "dd MMMM yyyy, HH:mm"
private const val LONG_YEAR_WITH_DAY_AND_SPACES = "dd MMMM yyyy, EEEE"
private const val MONTH_YEAR = "LLLL yyyy"
private const val MONTH = "LLLL"
private const val TIME = "HH:mm"

@SuppressLint("ConstantLocale")
object DateFormatUtil {
    val DATE_FORMAT = SimpleDateFormat(LONG_YEAR, Locale.getDefault())
    val DATE_FORMAT_SHORT_YEAR = SimpleDateFormat(SHORT_YEAR, Locale.getDefault())
    val DATE_FORMAT_WITH_TIME = SimpleDateFormat(LONG_YEAR_WITH_TIME, Locale.getDefault())
    val DATE_FORMAT_WITH_TIME_AND_SPACES = SimpleDateFormat(LONG_YEAR_WITH_TIME_AND_SPACES, Locale.getDefault())
    val DATE_FORMAT_WITH_DAY_AND_SPACES = SimpleDateFormat(LONG_YEAR_WITH_DAY_AND_SPACES, Locale.getDefault())
    val DATE_FORMAT_WITH_TIME_SHORT_YEAR = SimpleDateFormat(SHORT_YEAR_WITH_TIME, Locale.getDefault())
    val DATE_FORMAT_WITH_SPACES = SimpleDateFormat(LONG_YEAR_WITH_SPACES, Locale.getDefault())
    val DATE_FORMAT_MONTH_YEAR = SimpleDateFormat(MONTH_YEAR, Locale.getDefault())

    val LOCAL_DATE_FORMAT = DateTimeFormatter.ofPattern(LONG_YEAR, Locale.getDefault())
    val LOCAL_DATE_FORMAT_SHORT_YEAR = DateTimeFormatter.ofPattern(SHORT_YEAR, Locale.getDefault())
    val LOCAL_DATE_FORMAT_WITH_TIME = DateTimeFormatter.ofPattern(LONG_YEAR_WITH_TIME, Locale.getDefault())
    val LOCAL_DATE_FORMAT_WITH_TIME_SHORT_YEAR = DateTimeFormatter.ofPattern(SHORT_YEAR_WITH_TIME, Locale.getDefault())
    val LOCAL_DATE_FORMAT_WITH_SPACES = DateTimeFormatter.ofPattern(LONG_YEAR_WITH_SPACES, Locale.getDefault())
    val LOCAL_DATE_FORMAT_WITH_DAY_AND_SPACES = DateTimeFormatter.ofPattern(LONG_YEAR_WITH_DAY_AND_SPACES, Locale.getDefault())
    val LOCAL_DATE_FORMAT_WITH_TIME_AND_SPACES = DateTimeFormatter.ofPattern(LONG_YEAR_WITH_TIME_AND_SPACES, Locale.getDefault())
    val LOCAL_DATE_FORMAT_DAY_MONTH_SHORT = DateTimeFormatter.ofPattern(SHORT_DAY_MONTH, Locale.getDefault())
    val LOCAL_TIME_FORMAT = DateTimeFormatter.ofPattern(TIME, Locale.getDefault())
    val LOCAL_DATE_FORMAT_MONTH = DateTimeFormatter.ofPattern(MONTH, Locale.getDefault())
}