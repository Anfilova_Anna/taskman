package com.fila.taskman.presentation.base

import android.app.Dialog
import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import com.arellomobile.mvp.MvpAppCompatDialogFragment
import com.arellomobile.mvp.MvpView
import com.fila.taskman.ext.getUnknownErrorText
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

/**
 * Base fragment to use in the application.
 */
abstract class BaseFragment<TView : MvpView, TViewEvent, out TPresenter : BasePresenter<TView, TViewEvent>>
    : MvpAppCompatDialogFragment(), CoroutineScope {

    /**
     * Implementation of the scope's context
     * Job should be created when fragment is creating and canceled when fragment is destroying
     *
     * All uncaught exceptions will be caught by logExceptionHandler and logged.
     */
    private val job: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main/* + Dispatchers.logExceptionHandler(this::class.java.name) */

    private val errorView by lazy { SnackbarWrapper(activity as AppCompatActivity, this) }

    protected abstract val presenter: TPresenter

    // TODO: Potentially will be ignored orientation from manifest
    protected open fun needChangeOrientation() = false

    override fun onStart() {
        dialog?.let { dialog ->
            dialog.setOnShowListener {
                onDialogShown(dialog)
            }
        }
        super.onStart()
    }

    /**
     * Use this function instead of setting onShowListener to dialog directly.
     *
     // TODO
     */
    open fun onDialogShown(dialog: Dialog) {}

    override fun onDestroy() {
        job.cancel()

        super.onDestroy()
    }

    protected fun sendEvent(event: TViewEvent): Boolean = sendAnyEvent(event as Any)

    private fun sendAnyEvent(event: Any): Boolean =
            if (presenter.eventChannel.isClosedForSend) Timber.d("Event channel is closed for send.").let { false }
            else presenter.eventChannel.offer(event)

    protected fun setError(e: Throwable?, onOkClick: (() -> Any)? = null) {
        setError(getUnknownErrorText(e), onOkClick)
    }

    protected fun setError(@StringRes res: Int, onOkClick: (() -> Any)? = null) {
        setError(getString(res), onOkClick)
    }

    protected fun setError(e: String, onOkClick: (() -> Any)? = null) {
        errorView.apply {
            setErrorText(e)
            this.onOkClick = onOkClick
        }
    }
}