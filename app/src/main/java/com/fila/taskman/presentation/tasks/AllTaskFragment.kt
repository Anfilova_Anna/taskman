package com.fila.taskman.presentation.tasks

import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.fila.domain.model.Task
import com.fila.taskman.R
import com.fila.taskman.ext.setVisibility
import com.fila.taskman.presentation.base.BaseFragment
import com.fila.taskman.presentation.tasks.AllTasksPresenter.Event
import kotlinx.android.synthetic.main.fragment_task_main.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class AllTaskFragment : BaseFragment<MainTaskView, Event, AllTasksPresenter>(), MainTaskView {
    @InjectPresenter
    override lateinit var presenter: AllTasksPresenter

    private val adapter: AllTasksAdapter by lazy { AllTasksAdapter(activity!!) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater.inflate(R.layout.fragment_task_main, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

        launch {
            delay(3000)
            sendEvent(AllTasksPresenter.Event.GetTasks)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
//        inflater?.inflate(R.menu.setting_menu, menu).let { true }
    }


    override fun showMessage(@StringRes message: Int) {
        setError(message)
    }

    override fun showTasks(tasks: List<Task>) {
        adapter.setListData(tasks)
        fullScreenProgressView.setVisibility(false)
        emptyDataText.setVisibility(tasks.isEmpty())
    }

    override fun addNewOne(listId: Long) {
        val action = AllTaskFragmentDirections.actionAddTask()
        action.setListId(listId)
        findNavController(this).navigate(action)
    }

    private fun initView() {
        emptyDataText.setVisibility(adapter.itemCount == 0)
        fullScreenProgressView.setVisibility(true)
        recyclerView.layoutManager = LinearLayoutManager(activity!!)
        recyclerView.adapter = adapter

        addTaskBtn.setOnClickListener {
            sendEvent(Event.AddNewOne)
        }
    }
}