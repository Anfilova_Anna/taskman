package com.fila.taskman.presentation.base

import android.support.annotation.StringRes
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.MvpView
import com.fila.taskman.ext.getUnknownErrorText
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

abstract class BaseActivity<TView : MvpView, TViewEvent, TPresenter> : MvpAppCompatActivity(), CoroutineScope
        where TPresenter : BasePresenter<TView, TViewEvent> {

    abstract var presenter: TPresenter

    private val job: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private val errorView by lazy { SnackbarWrapper(this, this) }

    protected fun sendEvent(event: TViewEvent): Boolean = sendAnyEvent(event as Any)

    override fun onDestroy() {
        super.onDestroy()

        coroutineContext.cancel()
    }

    protected fun setError(e: Throwable?, onOkClick: (() -> Any)? = null) {
        setError(getUnknownErrorText(e), onOkClick)
    }

    protected fun setError(@StringRes res: Int, onOkClick: (() -> Any)? = null) {
        setError(getString(res), onOkClick)
    }

    protected fun setError(e: String, onOkClick: (() -> Any)? = null) {
        errorView.apply {
            setErrorText(e)
            this.onOkClick = onOkClick
        }
    }

    private fun sendAnyEvent(event: Any): Boolean =
            if (presenter.eventChannel.isClosedForSend) Timber.d("Event channel is closed for send.").let { false }
            else presenter.eventChannel.offer(event)
}