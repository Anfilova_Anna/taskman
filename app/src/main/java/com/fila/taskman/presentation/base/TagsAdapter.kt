package com.fila.taskman.presentation.base

import android.content.Context
import android.graphics.drawable.Drawable
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fila.taskman.R

class TagsAdapter(val context: Context): RecyclerAdapter<TagsAdapter.TagChip, TagsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(context).inflate(R.layout.chip_group, parent, false))

    override fun onBindViewHolder(view: ViewHolder, position: Int) {
//        view.chipView =  data[position].name
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
//        val chipView: ChipView = view.
    }

    data class TagChip(val name: String,
                       val id: Int,
                       val additionalInfo: String?)
// : ChipInterface {
//        override fun getInfo(): String? {
//            return additionalInfo
//        }
//
//        override fun getAvatarDrawable(): Drawable? {
//            return null
//        }
//
//        override fun getLabel(): String {
//            return name
//        }
//
//        override fun getId(): Any {
//            return id
//        }
//
//        override fun getAvatarUri(): Uri? {
//            return null
//        }
//    }
}