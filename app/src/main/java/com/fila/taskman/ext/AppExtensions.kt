package com.fila.taskman.ext

import android.app.Activity
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.content.ContextWrapper
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.support.annotation.StringRes
import android.support.design.widget.TabLayout
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.InputFilter
import android.util.TypedValue
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.fila.adapter.kotlin.coroutines.experimental.RetrofitException
import com.fila.taskman.R
import com.fila.taskman.presentation.utils.CustomTextWatcher
import com.fila.taskman.presentation.utils.DateFormatUtil
import org.threeten.bp.DayOfWeek
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.LocalTime
import org.threeten.bp.temporal.WeekFields
import timber.log.Timber
import java.io.IOException
import java.text.NumberFormat
import java.util.*

const val DEFAULT_EDIT_TEXT_DEBOUNCE_DELAY_MS = 500L

fun TextView.string() = text.toString()

fun Activity.hideKeyboard() {
    val view = this.currentFocus
    if (view != null) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

fun Fragment.hideKeyboard() {
    view?.let {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(it.windowToken, 0)
    }
}

fun TextView.onTextChanged(block: (String?) -> Unit) {
    addTextChangedListener(CustomTextWatcher { s: CharSequence?, _: Int, _: Int, _: Int -> block.invoke(s?.toString()) })
}

/**
 * Launches a job that will be started and cancelled respectively to [LifecycleOwner] events.
 *
 * @param startEvent Defines when the job will be started. [ON_START][Lifecycle.Event.ON_START]
 * by default.
 *
 * @param stopEvent Defines, when job will be cancelled. If nothing passed, then it cancels the job
 * on [startEvent] counterpart (i.e. for [ON_START][Lifecycle.Event.ON_START] the counterpart is
 * [ON_STOP][Lifecycle.Event.ON_STOP]
 *
 * @param context Coroutine context to execute passed code block on.
 *
 * @param startImmediately Defines if job has to be started immediately or it should wait for its
 * [startEvent] to fire.
 *
 * @param shouldRestart restart coroutine on start event of lifecycle owner.
 *
 * @param block Code that will be executed inside the job.
 */
//fun LifecycleOwner.launchAutoCancel(
//        startEvent: Lifecycle.Event = Lifecycle.Event.ON_START,
//        stopEvent: Lifecycle.Event? = null,
//        context: CoroutineContext = Dispatchers.Main,
//        startImmediately: Boolean = true,
//        shouldRestart: Boolean = true,
//        block: suspend () -> Unit): Job {
//
//    return JobLifecycleObserver(startEvent, stopEvent, context, startImmediately, shouldRestart, block)
//            .also { observer ->
//                lifecycle.addObserver(observer)
//                observer.mainJob.invokeOnCompletion {
//                    lifecycle.removeObserver(observer)
//                }
//            }
//            .mainJob
//}

fun View.setVisibility(isVisible: Boolean, setIfNotVisible: Int = View.GONE) {
    this.visibility = if (isVisible) View.VISIBLE else setIfNotVisible
}

fun View.isVisible(): Boolean {
    return this.visibility == View.VISIBLE
}

internal fun <T> Activity.showListPopup(adapter: ArrayAdapter<T>, anchor: View, itemsToShow: Int) {

    val window = ListPopupWindow(this)

    window.setAdapter(adapter)
    window.isModal = false
    window.anchorView = anchor
    window.verticalOffset = -anchor.measuredHeight

    var mostHeight = 0
    var mostWidth = 0

    val lv = ListView(this, null, android.R.attr.dropDownItemStyle)

    for (i in 0 until adapter.count) {
        val v = adapter.getView(i, null, lv)
        v.measure(0, 0)
        v.measuredHeight.takeIf { it > mostHeight }?.let { mostHeight = it }
        v.measuredWidth.takeIf { it > mostWidth }?.let { mostWidth = it }
    }

    (this as? AppCompatActivity)?.lifecycle?.addObserver(object : LifecycleObserver {

        @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
        fun onStop() {
            window.dismiss()
        }
    })

    window.width = mostWidth
    window.height = mostHeight * minOf(itemsToShow, adapter.count)

    window.horizontalOffset = -mostWidth + anchor.measuredWidth

    window.show()
}

/**
 * Setup toolbar with hamburger icon
 */

fun Toolbar.setupBaseToolbar(activity: FragmentActivity) {
    var tv: TextView? = null
    for (i in 0 until this.childCount) {
        val child = this.getChildAt(i)
        if (child is TextView) {
            tv = child
            break
        }
    }
    if (tv != null) {
        tv.typeface = Typeface.SANS_SERIF
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
    }
    with(activity as AppCompatActivity) {
        setSupportActionBar(this@setupBaseToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_menu)
    }
}

/**
 * Setup toolbar with back arrow icon
 */

fun Toolbar.setupHomeToolbar(activity: FragmentActivity) {
    var tv: TextView? = null
    for (i in 0 until this.childCount) {
        val child = this.getChildAt(i)
        if (child is TextView) {
            tv = child
            break
        }
    }
    if (tv != null) {
        tv.typeface = Typeface.SANS_SERIF
        tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
    }
    with(activity as AppCompatActivity) {
        setSupportActionBar(this@setupHomeToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
    }
}

fun Date?.format(isYearShort: Boolean = false) = when {
    this == null -> ""
    isYearShort -> DateFormatUtil.DATE_FORMAT_SHORT_YEAR.format(this)
    else -> DateFormatUtil.DATE_FORMAT.format(this)
}

fun Date?.formatWithTime(isYearShort: Boolean = false) = when {
    this == null -> ""
    isYearShort -> DateFormatUtil.DATE_FORMAT_WITH_TIME_SHORT_YEAR.format(this)
    else -> DateFormatUtil.DATE_FORMAT_WITH_TIME.format(this)
}

fun Date?.formatWithDayAndSpaces() = when {
    this == null -> ""
    else -> DateFormatUtil.DATE_FORMAT_WITH_DAY_AND_SPACES.format(this)
}

fun Date?.formatWithTimeAndSpaces() = when {
    this == null -> ""
    else -> DateFormatUtil.DATE_FORMAT_WITH_TIME_AND_SPACES.format(this)
}

fun LocalDate?.formatMonth() = when {
    this == null -> ""
    else -> DateFormatUtil.LOCAL_DATE_FORMAT_MONTH.format(this).capitalize()
}

fun LocalDate?.formatDayMonthShort() = when {
    this == null -> ""
    else -> DateFormatUtil.LOCAL_DATE_FORMAT_DAY_MONTH_SHORT.format(this)
}

fun Date?.formatWithSpaces() = when {
    this == null -> ""
    else -> DateFormatUtil.DATE_FORMAT_WITH_SPACES.format(this)
}

fun LocalDate?.formatWithSpaces() = when {
    this == null -> ""
    else -> DateFormatUtil.LOCAL_DATE_FORMAT_WITH_SPACES.format(this)
}

fun LocalDate?.format(isYearShort: Boolean = false) = when {
    this == null -> ""
    isYearShort -> format(DateFormatUtil.LOCAL_DATE_FORMAT_SHORT_YEAR)
    else -> format(DateFormatUtil.LOCAL_DATE_FORMAT)
}

fun Date?.formatMonthYear() = when {
    this == null -> ""
    else -> DateFormatUtil.DATE_FORMAT_MONTH_YEAR.format(this).capitalize()
}

fun LocalDateTime?.formatWithTime(isYearShort: Boolean = false) = when {
    this == null -> ""
    isYearShort -> format(DateFormatUtil.LOCAL_DATE_FORMAT_WITH_TIME_SHORT_YEAR)
    else -> format(DateFormatUtil.LOCAL_DATE_FORMAT_WITH_TIME)
}

fun LocalDateTime?.formatOnlyTime() = when {
    this == null -> ""
    else -> format(DateFormatUtil.LOCAL_TIME_FORMAT)
}

fun LocalDate?.formatWithDayAndSpaces() = when {
    this == null -> ""
    else -> DateFormatUtil.LOCAL_DATE_FORMAT_WITH_DAY_AND_SPACES.format(this)
}

fun LocalDateTime?.formatWithTimeAndSpaces() = when {
    this == null -> ""
    else -> DateFormatUtil.LOCAL_DATE_FORMAT_WITH_TIME_AND_SPACES.format(this)
}

fun LocalTime?.format() = when {
    this == null -> ""
    else -> format(DateFormatUtil.LOCAL_TIME_FORMAT)
}

fun String.trimZeroEnd(): String {
    val delimiter: Char?
    val parts: List<String>? =
            if (this.contains('.')) {
                delimiter = '.'
                this.split('.')
            } else if (this.contains(',')) {
                delimiter = ','
                this.split(',')
            } else {
                delimiter = null
                null
            }

    parts?.takeIf { parts.size == 2 }?.let {
        parts[1].let {
            var index: Int? = null

            for (i in it.length - 1 downTo 0) {
                if (it[i] != '0') {
                    index = i
                    break
                }
            }

            val resSecondPart = if (index == null) null else it.substring(0, index + 1)

            resSecondPart?.let {
                return parts[0].plus(delimiter).plus(resSecondPart)
            } ?: let {
                return parts[0]
            }
        }
    } ?: let {
        return this
    }
}

fun View.hideSoftKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(this.windowToken, 0)
}

fun EditText.showSoftKeyboard() {
    requestFocus()
    (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).showSoftInput(this, InputMethodManager.SHOW_FORCED)
}
//
//inline fun <T, reified R : Comparable<R>> Iterable<T>.sortedWithOrderBy(
//        orderType: OrderType,
//        crossinline selector: (T) -> R?): List<T> {
//
//    val isDate = R::class.java == Date::class.java
//
//    return if ((isDate && orderType == OrderType.DESC)
//            || (!isDate && orderType == OrderType.ASC)) {
//        sortedBy(selector)
//    } else {
//        sortedByDescending(selector)
//    }
//}

internal fun View.getActivity(): AppCompatActivity? {
    var context = context
    while (context is ContextWrapper) {
        if (context is AppCompatActivity) {
            return context
        }
        context = (context).baseContext
    }
    return null
}

fun NumberPicker.makeDividerTransparent() {
    val pickerFields = NumberPicker::class.java.declaredFields
    for (pf in pickerFields) {
        if (pf.name == "mSelectionDivider") {
            pf.isAccessible = true
            try {
                val colorDrawable = ColorDrawable(Color.TRANSPARENT)
                pf.set(this, colorDrawable)
                return
            } catch (e: Exception) {
                Timber.d(e, "Failed to make divider transparent")
            }
        }
    }
}

fun TimePicker.makeDividersTransparent() {
    val timeViewGroup = getChildAt(0) as ViewGroup?
    val ll = timeViewGroup?.getChildAt(0) as ViewGroup?
    val hourPicker = ll?.getChildAt(0) as NumberPicker?
    hourPicker?.makeDividerTransparent()
    val minutePicker = ll?.getChildAt(2) as NumberPicker?
    minutePicker?.makeDividerTransparent()
    val amPmPicker = timeViewGroup?.getChildAt(1) as NumberPicker?
    amPmPicker?.makeDividerTransparent()
}
//
//suspend fun EditText.subscribeTextChanged(debounceDelayMs: Long = DEFAULT_EDIT_TEXT_DEBOUNCE_DELAY_MS,
//                                          onTextChanged: (s: String) -> Any?) {
//    val channel = Channel<String>(Channel.CONFLATED)
//    val listener = CustomTextWatcher { s, _, _, _ -> channel.offer(s.toString()) }
//
//    addTextChangedListener(listener)
//    channel.invokeOnClose { removeTextChangedListener(listener) }
//
//    channel.debounce(debounceDelayMs)
//            .distinctUntilChanged()
//            .consumeEach {
//                onTextChanged(it)
//            }
//}

fun EditText.setTextWithSelection(text: String?) {
    if (getText().toString() == text) return

    val startSelection = selectionStart
    val endSelection = selectionEnd

    setText(text)

    text?.let {
        setSelection(
                if (startSelection == endSelection && endSelection < it.length) endSelection
                else it.length)
    }
}

/**
 * Fetch all children of ViewGroup recursively
 */
fun ViewGroup.getChildren(): List<View> {
    val children = mutableListOf<View>()
    for (i in 0 until childCount) {
        val child = getChildAt(i)
        if (child is ViewGroup) {
            children.addAll(child.getChildren())
        } else {
            children.add(child)
        }
    }
    return children
}

fun Context.getConnectivityErrorText(): String = resources.getString(R.string.base_connection_exception)

fun Context.getUnknownErrorText(
        ex: Throwable?,
        @StringRes defaultRes: Int = R.string.base_connection_exception): String = getUnknownErrorText(ex, defaultRes, resources)

fun Fragment.getUnknownErrorText(
        ex: Throwable?,
        @StringRes defaultRes: Int = R.string.base_connection_exception): String = getUnknownErrorText(ex, defaultRes, resources)

private fun getUnknownErrorText(ex: Throwable?, @StringRes defaultRes: Int, resources: Resources) = when (ex) {
    is RetrofitException -> ex.message
//    is MessageException -> ex.message
//    NetworkIsUnavailable,
    is IOException -> resources.getString(R.string.base_connection_exception)
    null -> ""
    else -> resources.getString(defaultRes)
}
//
//fun TextView.onTextChanged(block: (String?) -> Unit) {
//    addTextChangedListener(CustomTextWatcher { s: CharSequence?, _: Int, _: Int, _: Int -> block.invoke(s?.toString()) })
//}

fun String?.toNumberOrNull(): Number? = try {
    val format = NumberFormat.getInstance(Locale("ru", "RU"))
    format.parse(this?.trimZeroEnd())
} catch (e: Throwable) {
    null
}

@Deprecated(message = "This function doesn't replace '.' with ',' despite of requirements.",
        replaceWith = ReplaceWith("toCurrencyWithComma(context, defaultValue)"))
fun Double?.toCurrency(context: Context, defaultValue: Double? = 0.0): String =
        (this ?: defaultValue)?.let { context.getString(R.string.currency_format, it) } ?: ""

fun Double?.toCurrencyWithComma(context: Context, defaultValue: Double? = 0.0): String =
        (this ?: defaultValue)
                ?.let { context.getString(R.string.currency_format, it) }
                ?.replace(".", ",") ?: ""

fun Double?.toPercent(context: Context, defaultValue: String = ""): String =
        this?.let { context.getString(R.string.percent_format, it) } ?: defaultValue

fun Double?.toOneDecimalFormat(context: Context, defaultValue: Double? = 0.0): String =
        (this ?: defaultValue)?.let { context.getString(R.string.one_decimal_format, it) } ?: ""
//
//fun BaseActivity<*, *, *>.showTitledInfoDialog(titleText: String, @StringRes messageText: Int, @DrawableRes icon: Int? = null) {
//    getSupportFragmentManager().let {
//        it.findFragmentByTag(InfoDialog.TAG) as? InfoDialog
//                ?: InfoDialog.createTitledDialog(titleText, messageText, icon).apply { show(it, InfoDialog.TAG) }
//    }
//}

fun Lifecycle.Event.isCounterpartOf(event: Lifecycle.Event) =
        when (this) {
            Lifecycle.Event.ON_CREATE -> Lifecycle.Event.ON_DESTROY
            Lifecycle.Event.ON_START -> Lifecycle.Event.ON_STOP
            Lifecycle.Event.ON_RESUME -> Lifecycle.Event.ON_PAUSE
            Lifecycle.Event.ON_PAUSE -> Lifecycle.Event.ON_RESUME
            Lifecycle.Event.ON_STOP -> Lifecycle.Event.ON_START
            Lifecycle.Event.ON_DESTROY -> Lifecycle.Event.ON_CREATE
            else -> null
        } == event

fun String?.toDoubleWithDividerCorrection(): Double? = this?.takeIf { it.isNotBlank() }?.replace(',', '.')?.toDouble()

fun Editable?.toIntOrNull(): Int? = this?.takeIf { it.isNotBlank() }?.toString()?.toInt()

fun CharSequence?.toIntOrNull(): Int? = this?.takeIf { it.isNotBlank() }?.toString()?.toInt()

inline fun <reified T> DialogFragment.getDialogListener(): T? {
    return parentFragment as? T ?: activity as? T
}

fun RadioGroup.getCheckedText(): String? =
        checkedRadioButtonId.takeIf { it > 0 }?.let {
            findViewById<RadioButton>(it).text.toString().trim()
        }

fun Activity.isPortrait(): Boolean {
    val manager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
    return when (manager.defaultDisplay.rotation) {
        Surface.ROTATION_0, Surface.ROTATION_180 -> true
        else -> false
    }
}

fun TabLayout.setTabWidthAsWrapContent(tabPosition: Int) {
    val layout = (this.getChildAt(0) as LinearLayout).getChildAt(tabPosition) as LinearLayout
    val layoutParams = layout.layoutParams as LinearLayout.LayoutParams
    layoutParams.weight = 0f
    layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
    layout.layoutParams = layoutParams
}

// TODO remove after Kotlin 1.3 migration
fun <T> Array<T>?.isNullOrEmpty(): Boolean {
    return (this == null || this.isEmpty())
}

fun LocalDate.getWeek(): Pair<LocalDate, LocalDate> = with(DayOfWeek.MONDAY) to with(DayOfWeek.SUNDAY)

fun LocalDate.getWeekNumber(): Int =
        this.get(WeekFields.of(Locale.getDefault()).weekOfWeekBasedYear())

fun EditText.setMaxTextLength(maxLength: Int) {
    val filters = this.filters
    val filterLength = InputFilter.LengthFilter(maxLength)

    this.filters = if (!filters.isNullOrEmpty()) {
        var isInFilters = false

        filters.forEachIndexed { index, inputFilter ->
            if (inputFilter is InputFilter.LengthFilter) {
                filters[index] = filterLength
                isInFilters = true
                return@forEachIndexed
            }
        }

        if (isInFilters) filters
        else filters.toMutableList().apply { add(filterLength) }.toTypedArray()
    } else arrayOf(filterLength)
}
//
//fun EditText.setMinMaxValue(minValue: Int, maxValue: Int) {
//    val filters = this.filters
//    val filterMinMax = MinMaxFilter(minValue, maxValue)
//
//    this.filters = if (!filters.isNullOrEmpty()) {
//        var isInFilters = false
//
//        filters.forEachIndexed { index, inputFilter ->
//            if (inputFilter is MinMaxFilter) {
//                filters[index] = filterMinMax
//                isInFilters = true
//                return@forEachIndexed
//            }
//        }
//
//        if (isInFilters) filters
//        else filters.toMutableList().apply { add(filterMinMax) }.toTypedArray()
//    } else arrayOf(filterMinMax)
//}

inline fun <T : View> T.afterMeasured(crossinline block: T.() -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            if (measuredWidth > 0 && measuredHeight > 0) {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                block()
            }
        }
    })
}