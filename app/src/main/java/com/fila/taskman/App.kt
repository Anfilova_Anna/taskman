package com.fila.taskman

import android.app.Application
import com.arellomobile.mvp.MvpFacade
import com.facebook.stetho.Stetho
import com.fila.taskman.di.modules
import com.google.firebase.FirebaseApp
import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.android.ext.android.startKoin
import timber.log.Timber

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        MvpFacade.init()
        startKoin(this, modules)
        FirebaseApp.initializeApp(applicationContext)
        AndroidThreeTen.init(applicationContext)
        Stetho.initializeWithDefaults(applicationContext) // TODO do not initialise for release version!!
    }
}