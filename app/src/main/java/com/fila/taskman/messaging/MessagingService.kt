package com.fila.taskman.messaging

import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber

class MessagingService: FirebaseMessagingService() {

    companion object {
        val TAG = MessagingService::class.java.name

        val ACTION = "ru.nurafortochkina.chat.services.NEW_MESSAGE"

        val MESSAGE_KEY = "message"
        val USERNAME_KEY = "username"
    }

    override fun onNewToken(token: String?) {
        //todo compare token with FirebaseInstanceId.getInstance().getInstanceId()
        Timber.w("TAGGGG get instanceId: ${FirebaseInstanceId.getInstance().instanceId}")
        Timber.w("TAGGGG new token $token")
    }

    override fun onMessageReceived(message: RemoteMessage?) {
        Timber.w("onMessageReceived: $message.")
        if (message!!.data == null)
            return

        val messageText = message.data[MESSAGE_KEY]
        val username = message.data[USERNAME_KEY]

        Timber.w("message: $messageText")
        Timber.w("username: $username")

        val intent = Intent(ACTION)
        intent.putExtra(MESSAGE_KEY, messageText)
        intent.putExtra(USERNAME_KEY, username)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }
}