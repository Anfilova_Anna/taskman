package com.fila.taskman.di

import android.arch.persistence.room.Room
import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import com.fila.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import com.fila.data.Api
import com.fila.data.MessageApi
import com.fila.data.TaskApi
import com.fila.data.adapter.LocalDateTypeAdapter
import com.fila.data.db.DATABASE_NAME
import com.fila.data.db.MigrationSet
import com.fila.data.db.TaskManDb
import com.fila.data.dbmapper.TagDbMapper
import com.fila.data.dbmapper.TaskDbMapper
import com.fila.data.mapper.TagMapper
import com.fila.data.mapper.TaskMapper
import com.fila.data.repository.TaskRepositoryImpl
import com.fila.domain.interactor.TaskInteractor
import com.fila.domain.repository.TaskRepository
import com.fila.taskman.presentation.base.SnackbarWrapper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.module
import org.threeten.bp.LocalDate
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.x5.gkmobile.data.api.converter.DateToStringConverterFactory
import ru.x5.gkmobile.data.api.converter.LocalDateToStringConverterFactory
import ru.x5.gkmobile.data.api.converter.UnitConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit


private val applicationModule = module {
//    single { sharedPreferences(get()) }
    single { gson() }
    single { okHttp() }
    single { retrofit(get(), get()) }
    single { Api(get(), get()) }
    single { taskManDb(get()) }
    factory { (activity: AppCompatActivity) -> SnackbarWrapper(activity) }
//    single { DomainLogger() as Logger }
}

private val apiModule = module {
    factory { get<Retrofit>().create(TaskApi::class.java) }
    factory { get<Retrofit>().create(MessageApi::class.java) }
}

//private val interceptorsModule = module {
//    factory { EndpointInterceptor(get()) }
//}

private val interactorModule = module {
    factory { TaskInteractor(get()) }
}

private val repositoryModule = module {
    single { TaskRepositoryImpl(get(), get(), get() ,get()) as TaskRepository }
//    single(RequestMark::class.java.name) { SharedDataRepositoryImpl<List<RequestMark>>() as SharedDataRepository<List<RequestMark>> }
}

private val mapperModule = module {
    single { TaskMapper(get()) }
    single { TaskDbMapper(get()) }
    single { TagMapper() }
    single { TagDbMapper() }
}

private val platformModule = module {
//    single { ConnectivityImpl(get(), get()) as Connectivity }
//    single { AppStateImpl() as AppState }
}

private val presentersModule = module {
//    viewModel { TaskPre(get(), get()) }
}

//private val schedulersModule = module {
//    factory { AlarmSchedulingManagerImpl(get()) as SchedulingManager }
//}

//private val homeModule = module {
//    module(GlobalScreen.Home.name) {
//        single(KoinProviderName.HOME.name) { Cicerone.create(HomeRouter()) as Cicerone<HomeRouter> }
//        single(KoinProviderName.HOME.name) {
//            get<Cicerone<HomeRouter>>(KoinProviderName.HOME.name).router
//                    as HomeRouter
//        }
//        single(KoinProviderName.HOME.name) {
//            get<Cicerone<HomeRouter>>(KoinProviderName.HOME.name)
//                    .navigatorHolder
//        }
//    }
//}

val modules = listOf(
        applicationModule,
        apiModule,
        presentersModule,
        interactorModule,
        repositoryModule,
        mapperModule,
        platformModule)

enum class KoinProviderName {
    GLOBAL,
    HOME;
}

private object PrivateConstants {
    const val commonPreferences = "common"
    const val baseUrl = "http://taskman.ddns.net:3000/"
}

fun sharedPreferences(context: Context) =
        context.getSharedPreferences(PrivateConstants.commonPreferences, Context.MODE_PRIVATE) as SharedPreferences

fun okHttp(): OkHttpClient {

    val logging = HttpLoggingInterceptor { Timber.d(it) }

    logging.level = HttpLoggingInterceptor.Level.BODY

    return OkHttpClient.Builder()
            .addInterceptor(logging)
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()
}

fun retrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit = Retrofit.Builder()
        .addCallAdapterFactory(CoroutineCallAdapterFactory(gson))
        .addConverterFactory(UnitConverterFactory)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addConverterFactory(DateToStringConverterFactory(gson))
        .addConverterFactory(LocalDateToStringConverterFactory(gson))
        .client(okHttpClient)
        .baseUrl(PrivateConstants.baseUrl)
        .build()

private fun gson() = GsonBuilder()
        .registerTypeAdapter(LocalDate::class.java, LocalDateTypeAdapter("dd.MM.yyyy HH:mm"))
        .setLenient()
        .setDateFormat("dd.MM.yyyy HH:mm")
        .create()

private fun taskManDb(context: Context) = Room.databaseBuilder(context.applicationContext,
        TaskManDb::class.java, DATABASE_NAME)
        .addMigrations(*MigrationSet.migrationSet)
        .fallbackToDestructiveMigration() // TODO
        .build()